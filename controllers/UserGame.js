const { UserGame } = require("../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const salt = bcrypt.genSaltSync(10);
const generateJWT = require("../helper/jwt.helper");
const cloudinary = require("../services/cloudinary.service");
const fs = require("fs");
const { OAuth2Client } = require("google-auth-library");
const axios = require("axios");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const sendEmail = require("../services/sendEmail.service");
const otpGenerator = require("otp-generator");

class UserGameController {
  static login = async (req, res, next) => {
    try {
      if (req.body.email && req.body.password) {
        const user = await UserGame.findOne({
          where: {
            email: req.body.email,
          },
        });

        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }
        if (bcrypt.compareSync(req.body.password, user.password)) {
          console.log(user);
          const token = generateJWT(user);

          res.status(200).json({
            token,
          });
        } else {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }
      } else if (req.body.google_id_token) {
        // melakukan verifikasi id token
        const payload = await client.verifyIdToken({
          idToken: req.body.google_id_token,
          requiredAudience: process.env.GOOGLE_CLIENT_ID,
        });
        // mencari email dari google di database
        const user = await UserGame.findOne({
          where: {
            email: payload.payload.email,
          },
        });

        if (user) {
          const token = generateJWT(user);

          res.status(200).json({ token });
        } else {
          const createdUser = await UserGame.create({
            email: payload.payload.email,
          });
          const token = generateJWT(createdUser);

          const html = `
          <pre>
            halo, terimakasih telah mendaftar :)
          <pre>
        `;

          await sendEmail(
            "naufalcorp@gmail.com",
            createdUser.email,
            html,
            null,
            "Registration complete"
          );

          res.status(200).json({ token });
        }
        // mendaftarkan secara otomatis, jika user belom ada di database
      } else if (req.body.facebook_id_token) {
        const response = await axios.get(
          `https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`
        );
        // mencari user di database
        const user = await UserGame.findOne({
          where: {
            email: response.data.email,
          },
        });

        if (user) {
          const token = generateJWT(user);

          res.status(200).json({ token });
        } else {
          const createdUser = await UserGame.create({
            email: response.data.email,
          });

          const token = generateJWT(createdUser);

          const html = `
          <pre>
            halo, terimakasih telah mendaftar :)
          <pre>
        `;

          await sendEmail(
            "naufalcorp@gmail.com",
            createdUser.email,
            html,
            null,
            "Registration complete"
          );

          res.status(200).json({ token });
        }
      } else {
        throw {
          status: 401,
          message: "Invalid email or password",
        };
      }
    } catch (err) {
      next(err);
    }
  };

  // register user
  static create = async (req, res, next) => {
    const { email, password } = req.body;

    try {
      const user = await UserGame.findOne({
        where: {
          email: email,
        },
      });

      // console.log(user);
      if (user) {
        throw {
          status: 400,
          message: "Email already in use",
        };
      }

      const result = await cloudinary.uploader.upload(req.file.path, {
        resource_type: "video",
      });

      // menghapus gambar di dalam folder public/src
      console.log(req.file.path);
      fs.unlinkSync(req.file.path);

      // console.log(result);

      const data = await UserGame.create({
        email: email,
        password: bcrypt.hashSync(password, salt),
        video: result.secure_url,
      });

      const html = `
      <pre>
      halo, terimakasih telah mendaftar :)
      <pre>
      `;

      await sendEmail(
        "naufalcorp@gmail.com",
        email,
        html,
        null,
        "Registration complete"
      );

      res.status(201).json({ message: "Succesfully create usergame" });
    } catch (error) {
      next(error);
    }
  };

  static findAll = async (req, res, next) => {
    try {
      const data = await UserGame.findAll({
        attributes: { exclude: ["password"] },
      });

      if (data.length > 0) {
        res.status(200).json(data);
      } else {
        res.status(200).json({ message: "Empty Collection" });
      }
    } catch (error) {
      // console.log()
      next(error);
    }
  };

  static find = async (req, res, next) => {
    try {
      const data = await UserGame.findOne({
        where: { id: req.params.id },
        attributes: { exclude: ["password"] },
        include: ["biodata", "history"],
      });

      if (data) {
        res.status(200).json(data);
      } else {
        throw {
          status: 404,
          message: "Resource not found",
        };
      }
    } catch (error) {
      next(error);
    }
  };

  static update = async (req, res, next) => {
    try {
      console.log("test");
      const data = await UserGame.update(req.body, {
        where: { id: req.params.id },
        returning: true,
      });
      res.status(200).json({ message: "Successfully update usergame" });
    } catch (error) {
      next(error);
    }
  };

  static destroy = async (req, res, next) => {
    try {
      await UserGame.destroy({ where: { id: req.params.id } });
      res.status(200).json({ message: "Successfully delete usergame" });
    } catch (error) {
      next(error);
    }
  };

  static async sendForgotPasswordToken(req, res, next) {
    // user mengirimkan alamat email
    // cek apakah email terdaftar di aplikasi
    // kita generate token (OTP) => pakai otp-generator
    // simpan otp ke database, dan kita tentukan expirednya
    // kirim email

    try {
      const user = await UserGame.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (!user) {
        throw {
          status: 400,
          message: "Invalid email",
        };
      } else {
        const otp = otpGenerator.generate(6, {
          upperCaseAlphabets: false,
          specialChars: false,
        });
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(otp, salt);
        const updated = await UserGame.update(
          {
            forgot_pass_token: hash,
            forgot_pass_token_expired_at: new Date(
              new Date().getTime() + 5 * 60000
            ),
          },
          {
            where: {
              email: req.body.email,
            },
          }
        );

        const html = `
        <pre>
        Token Anda: ${otp} <br>
        Email terbuat otomatis pada ${new Date()}
        <pre>
        `;
        await sendEmail(
          "naufalcorp@gmail.com",
          req.body.email,
          html,
          null,
          "Your Forgot Password Token"
        );
        res.status(200).json({
          message: "Succesfully send email",
        });
      }
    } catch (err) {
      next(err);
    }
  }

  static async verifyForgotPasswordToken(req, res, next) {
    try {
      const user = await UserGame.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (bcrypt.compareSync(req.body.token, user.forgot_pass_token)) {
        if (user.forgot_pass_token_expired_at > new Date()) {
          res.status(200).json({
            valid: true,
            message: "Token is valid",
          });
        } else {
          throw {
            status: 400,
            message: "Invalid token",
          };
        }
      } else {
        throw {
          status: 400,
          message: "Invalid token",
        };
      }
    } catch (err) {
      next(err);
    }
  }

  static async changePassword(req, res, next) {
    // password dan password confirmation => BE dan FE
    // FE juga harus mengirimkan email dan token
    try {
      if (req.body.password === req.body.password_confirmation) {
        const user = await UserGame.findOne({
          where: {
            email: req.body.email,
          },
        });
        if (user) {
          console.log(req.body.token, user.forgot_pass_token);
          if (bcrypt.compareSync(req.body.token, user.forgot_pass_token)) {
            if (user.forgot_pass_token_expired_at > new Date()) {
              const salt = bcrypt.genSaltSync(10);
              const hash = bcrypt.hashSync(req.body.password, salt);
              await UserGame.update(
                {
                  password: hash,
                  forgot_pass_token: null,
                  forgot_pass_token_expired_at: null,
                },
                {
                  where: {
                    email: req.body.email,
                  },
                }
              );
              res.status(200).json({
                message: "Successfully change password",
              });
            } else {
              throw {
                status: 400,
                message: "Invalid user or token",
              };
            }
          } else {
            throw {
              status: 400,
              message: "Invalid user or token",
            };
          }
        } else {
          throw {
            status: 400,
            message: "Invalid user or token",
          };
        }
      } else {
        throw {
          status: 400,
          message: "Password does not match password confirmation",
        };
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = UserGameController;
