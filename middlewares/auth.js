const passport = require("../config/passport");

const auth = passport.authenticate("jwt", {
  session: false,
});

module.exports = auth;

// const jwt = require("jsonwebtoken");
// const { UserGame } = require("../models");

// const auth = async (req, res, next) => {
//   try {
//     const token = req.headers.authorization;
//     if (!token) {
//       res.status(401).json({
//         message: "Unauthorized request",
//       });
//     } else {
//       const decoded = jwt.verify(token, "key");

//       const user = await UserGame.findOne({
//         where: { id: decoded.id },
//         attributes: ["id", "username"],
//       });

//       // const test = await UserGame.findAll();

//       if (user) {
//         req.user = decoded;
//         next();
//       } else {
//         res.status(401).json({
//           message: "Unauthorized request",
//         });
//       }
//     }
//   } catch (error) {
//     next({ status: 401, message: "Unauthorized request" });
//   }
// };

// module.exports = auth;
