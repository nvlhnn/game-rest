"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.UserGameBiodata, {
        foreignKey: "userGameId",
        as: "biodata",
      });
      this.hasOne(models.UserGameHistory, {
        foreignKey: "userGameId",
        as: "history",
      });
    }
  }
  UserGame.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      video: DataTypes.STRING,
      forgot_pass_token: DataTypes.STRING,
      forgot_pass_token_expired_at: DataTypes.DATE,
    },
    {
      sequelize,
      tableName: "user_games",
      modelName: "UserGame",
    }
  );
  return UserGame;
};
