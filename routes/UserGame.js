const UserGameController = require("../controllers/UserGame");
const validate = require("../validation/validator");
const { userGameSchema, loginSchema } = require("../validation/userGame");
const auth = require("../middlewares/auth");
const isUser = require("../middlewares/isUser");
// const auth = require("../middlewares/auth");
const router = require("express").Router();

const multer = require("multer");
const storage = require("../services/multerStorage.service");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "video/x-flv" ||
      file.mimetype === "video/mp4" ||
      file.mimetype === "video/3gpp"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be a video"), false);
    }
  },
});

router.get("/login", UserGameController.login);

router.post(
  "/",
  upload.single("video"),
  validate(userGameSchema),
  UserGameController.create
);
router.put(
  "/:id",
  [auth, isUser, upload.single("video")],
  validate(userGameSchema),
  UserGameController.update
);
router.get("/", [auth], UserGameController.findAll);

router.get("/:id", [auth], UserGameController.find);

router.delete("/:id", [auth, isUser], UserGameController.destroy);

router.post(
  "/send-forgot-pass-token",
  UserGameController.sendForgotPasswordToken
);
router.post(
  "/verify-forgot-pass-token",
  UserGameController.verifyForgotPasswordToken
);
router.post("/change-pass", UserGameController.changePassword);

module.exports = router;
