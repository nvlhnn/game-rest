const router = require("express").Router();

const userGameRoute = require("./UserGame");

router.use("/user-games", userGameRoute);

router.use("/api", router);
// router.get('api/user/stat', (req, res) => res.status(200).json('ok'))
router.get("/api", (req, res) => res.status(404).json("No API route found"));

module.exports = router;
