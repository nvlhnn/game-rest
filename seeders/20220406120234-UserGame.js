"use strict";
const bcrypt = require("bcryptjs");
const salt = bcrypt.genSaltSync(10);
const { sequelize } = require("../models/index");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "user_games",
      [
        {
          email: "oktav@gmail.com",
          password: bcrypt.hashSync("oktav", salt),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games", null, {});
    await sequelize.query(
      "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
    );
  },
};
