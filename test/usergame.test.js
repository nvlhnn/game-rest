const request = require("supertest");
const app = require("../index.js");
const { sequelize } = require("../models/index");
const { queryInterface, query } = sequelize;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { UserGame } = require("../models");
const generateJWT = require("../helper/jwt.helper.js");

let token;
let token2;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("Oktav123", salt);
  await queryInterface.bulkInsert(
    "user_games",
    [
      {
        email: "oktav@gmail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        email: "oktav2@gmail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    {}
  );
  token = generateJWT({ id: 2, email: "oktav2@gmail.com" });
  token2 = generateJWT({ id: 2, email: "oktav2@gmail.com" });
});

afterEach(async () => {
  // await queryInterface.bulkDelete(
  //   "user_games",
  //   {},
  //   { truncate: { cascade: true }, restartIdentity: true }
  // );

  await sequelize.query(
    "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
  );
});

describe("GET a Usergame", () => {
  it("success", (done) => {
    console.log(token);
    request(app)
      .get("/api/user-games/1")
      .set("authorization", token)

      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body instanceof Object).toBe(true);
          expect(res.body).toHaveProperty("id");
          expect(res.body).toHaveProperty("email");
          expect(res.body).toHaveProperty("createdAt");
          expect(res.body).toHaveProperty("updatedAt");
          expect(res.body).toHaveProperty("biodata");
          expect(res.body).toHaveProperty("history");
          done();
        }
      });
  });

  it("invalid id", async () => {
    // await sequelize.query(
    //   "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
    // );

    const res = await request(app)
      .get("/api/user-games/3")
      .set("authorization", token);
    expect(res.status).toBe(404);
    expect(res.body).toHaveProperty("message");
    expect(res.body.message).toBe("Resource not found");
  });

  it("no auth", (done) => {
    request(app)
      .get("/api/user-games/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/api/user-games/1")
      .set("authorization", "qweqwe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("expired token", async () => {
    await sequelize.query(
      "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
    );
    const res = await request(app)
      .get("/api/user-games/1")
      .set("authorization", token);
    expect(res.status).toBe(401);
    expect(res.body).toHaveProperty("message");
    expect(res.body.message).toBe("Unauthorized request");
  });
});

describe("GET all Usergame", () => {
  it("success", (done) => {
    request(app)
      .get("/api/user-games")
      .set("authorization", token)

      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(Array.isArray(res.body)).toBe(true);
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .get("/api/user-games")
      .set("authorization", "qweqwe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .get("/api/user-games")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
});

describe("POST Todo", () => {
  it("success", (done) => {
    request(app)
      .post("/api/user-games")
      .set("authorization", token)
      .send({
        email: "oktav5@gmail.com",
        password: "Naufal123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Succesfully create usergame");
          done();
        }
      });
  });

  it("required field violation", (done) => {
    request(app)
      .post("/api/user-games")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(422);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toHaveProperty("email");
          expect(res.body.message).toHaveProperty("password");
          expect(res.body.message.email.includes("Email is required")).toBe(
            true
          );
          expect(
            res.body.message.password.includes("Password is required")
          ).toBe(true);
          done();
        }
      });
  });

  it("email violation", (done) => {
    request(app)
      .post("/api/user-games")
      .set("authorization", token)
      .send({
        email: "oktav@gmail.com",
        password: "Oktav1232",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(422);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toHaveProperty("email");
          expect(res.body.message).toHaveProperty("password");
          expect(res.body.message.email.includes("Email Already Taken")).toBe(
            true
          );
          done();
        }
      });
  });
});

describe("UPDATE /todos/:id", () => {
  it("success", (done) => {
    request(app)
      .put("/api/user-games/1")
      .set("authorization", token)
      .send({
        email: "oktav21@gmail.com",
        password: "Oktav123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully update usergame");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .put("/api/user-games/1")
      .set("authorization", "qweqwe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .put("/api/user-games/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("required field violation", (done) => {
    request(app)
      .put("/api/user-games/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(422);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toHaveProperty("email");
          expect(res.body.message).toHaveProperty("password");
          expect(res.body.message.email.includes("Email is required")).toBe(
            true
          );
          expect(
            res.body.message.password.includes("Password is required")
          ).toBe(true);
          done();
        }
      });
  });

  it("updated by another user", (done) => {
    request(app)
      .put("/api/user-games/1")
      .set("authorization", token2)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(403);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Forbidden request");
          done();
        }
      });
  });
});

describe("DELETE Todo", () => {
  it("success", (done) => {
    request(app)
      .delete("/api/user-games/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully delete usergame");
          done();
        }
      });
  });

  it("invalid token", (done) => {
    request(app)
      .delete("/api/user-games/1")
      .set("authorization", "qweqwe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("no auth", (done) => {
    request(app)
      .delete("/api/user-games/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("deleted by another user", (done) => {
    request(app)
      .delete("/api/user-games/1")
      .set("authorization", token2)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(403);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Forbidden request");
          done();
        }
      });
  });
});

describe("Login API", () => {
  it("success", (done) => {
    request(app)
      .get("/user-games/login")
      .send({
        email: "oktav@gmail.com",
        password: "Oktav123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("token");
          done();
        }
      });
  });

  it("wrong email", (done) => {
    request(app)
      .get("/user-games/login")
      .send({
        email: "hanan@gm.com",
        password: "Oktav123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid email or password");
          done();
        }
      });
  });

  it("wrong password", (done) => {
    request(app)
      .get("/user-games/login")
      .send({
        email: "oktav@gmail.com",
        password: "hanan123",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Invalid email or password");
          done();
        }
      });
  });
});
