const { check, body } = require("express-validator");
const { UserGame } = require("../models");

const userGameSchema = [
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .bail()
    .isLength({ min: 4 })
    .withMessage("Email must at least 4 character")
    .custom((value) => {
      return UserGame.findOne({ where: { email: value } }).then((user) => {
        if (user) {
          return Promise.reject("Email Already Taken");
        }
      });
    }),
  check("password")
    .notEmpty()
    .withMessage("Password is required")
    .bail()
    .isLength({ min: 8 })
    .withMessage("Password Must Be at Least 8 Characters")
    .matches("[0-9]")
    .withMessage("Password Must Contain a Number")
    .matches("[A-Z]")
    .withMessage("Password Must Contain an Uppercase Letter")
    .trim()
    .escape(),
];

const loginSchema = [
  check("email")
    .if(
      body("google_id_token").not().exists() &&
        body("facebook_id_token").not().exists()
    )
    // .if(body("facebook_id_token").not().exists())
    .notEmpty()
    .withMessage("Email is required")
    .bail()
    .isLength({ min: 4 })
    .withMessage("Email must at least 4 character")
    .custom((value) => {
      return UserGame.findOne({ where: { email: value } }).then((user) => {
        if (user) {
          return Promise.reject("Email Already Taken");
        }
      });
    }),
  check("password")
    .if((value, { req }) => {
      return !req.body.google_id_token && !req.body.facebook_id_token;
    })
    .notEmpty()
    .withMessage("Password is required")
    .bail()
    .isLength({ min: 8 })
    .withMessage("Password Must Be at Least 8 Characters")
    .matches("[0-9]")
    .withMessage("Password Must Contain a Number")
    .matches("[A-Z]")
    .withMessage("Password Must Contain an Uppercase Letter")
    .trim()
    .escape(),
];

module.exports = { userGameSchema, loginSchema };
